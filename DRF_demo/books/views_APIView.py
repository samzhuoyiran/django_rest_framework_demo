import json

from rest_framework.response import Response
from rest_framework.views import APIView

from books.models import BookInfo
from books.serializers import BookInfoSerializer


# Create your views here.


class BookView(APIView):

    # 返回单一数据
    def get(self, request, id):

        try:
            book = BookInfo.objects.get(id=id)
        except:
            return Response({'errors': '数据获取失败'})

        return Response({
            'btitle': book.btitle,
            'bcomment': book.bcomment,
            'bread': book.bread,
            'bpub_date': book.bpub_date
        })

    # 修改数据
    def put(self, request, id):

        try:
            book = BookInfo.objects.get(id=id)
        except:
            return Response({'errors': '数据获取失败'})

        # 获取修改的数据
        json_str = request.data.decode()
        json_dict = json.loads(json_str)

        # 数据更新
        book.btitle = json_dict['btitle']
        book.bpub_date = json_dict['bpub_date']
        book.save()

        return Response({
            'btitle': book.btitle,
            'bcomment': book.bcomment,
            'bread': book.bread,
            'bpub_date': book.bpub_date
        })

    # 删除数据
    def delete(self, request, id):

        try:
            book = BookInfo.objects.get(id=id)
        except:
            return Response({'errors': '数据获取失败'})

        book.delete()

        return Response({'success': '删除成功'})


class BookListView(APIView):

    # 返回所有数据
    def get(self, request):
        books = BookInfo.objects.all()
        books_ser = BookInfoSerializer(books, many=True)

        return Response(books_ser.data)

    # 请求保存数据
    def post(self, request):
        json_str = request.data
        print(json_str)

        # 生成序列化对象
        book_ser = BookInfoSerializer(data=json_str)
        book_ser.is_valid(raise_exception=True)

        book_ser.save()

        return Response(book_ser.data)
