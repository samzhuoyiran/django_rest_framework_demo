import json
from datetime import datetime

from django.http import JsonResponse
from django.views.generic.base import View

from books.models import BookInfo


class BooksView(View):

    # 请求所有数据
    def get(self, request):
        books = BookInfo.objects.all()
        data_list = []

        for book in books:
            data_list.append({
                'btitle': book.btitle,
                'bpub_date': book.bpub_date,
                'bread': book.bread,
                'bcomment': book.bcomment
            })

        return JsonResponse(data_list, safe=False)

    # 请求保存数据
    def post(self, request):
        json_str = request.body
        json_str = json_str.decode()
        json_dict = json.loads(json_str)

        book = BookInfo.objects.create(
            btitle=json_dict['btitle'],
            bpub_date=datetime.strptime(json_dict['bpub_date'], '%Y-%m-%d').date()
        )

        return JsonResponse({
            'btitle': book.btitle,
            'bpub_date': book.bpub_date,
            'bread': book.bread,
            'bcomment': book.bcomment
        })


class BookView(View):

    # 获取单一数据
    def get(self, request, id):
        # 获取数据 --> 捕获异常
        try:
            book = BookInfo.objects.get(id=id)
        except:
            return JsonResponse({'error': '数据获取失败'})

        return JsonResponse({
            'btitle': book.btitle,
            'bpub_date': book.bpub_date,
            'bread': book.bread,
            'bcomment': book.bcomment
        })

    # 修改数据
    def put(self, request, id):
        # 获取数据 --> 捕获异常
        try:
            book = BookInfo.objects.get(id=id)
        except:
            return JsonResponse({'error': '数据获取失败'})

        # 获取需要修改的数据
        json_str = request.body
        json_str = json_str.decode()
        json_dict = json.loads(json_str)

        # 更新数据
        book.btitle = json_dict['btitle']
        book.bpub_date = datetime.strptime(json_dict['bpub_date'], '%Y-%m-%d').date()
        book.save()

        return JsonResponse({
            'btitle': book.btitle,
            'bpub_date': book.bpub_date,
            'bread': book.bread,
            'bcomment': book.bcomment
        })

    # 删除数据
    def delete(self, request, id):
        # 获取数据 --> 捕获异常
        try:
            book = BookInfo.objects.get(id=id)
        except:
            return JsonResponse({'error': '数据获取失败'})

        book.delete()

        return JsonResponse({'sucess': '删除成功'})
