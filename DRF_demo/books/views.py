from rest_framework.generics import ListCreateAPIView,RetrieveUpdateDestroyAPIView
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from rest_framework.decorators import action
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.throttling import UserRateThrottle
from rest_framework.filters import OrderingFilter

from books.models import BookInfo
from books.serializers import BookInfoSerializer

class Page(PageNumberPagination):
    page_size = 5
    # page_size_query_param = 'page_size'
    max_page_size = 10


class BooksView(ListCreateAPIView):

    # 指定查询集＆序列化器
    queryset = BookInfo.objects.all()
    serializer_class = BookInfoSerializer

class BookView(RetrieveUpdateDestroyAPIView):

    queryset = BookInfo.objects.all()
    serializer_class = BookInfoSerializer
    # 查询单一数据库对象时使用的条件字段，默认为pk
    # lookup_field = 'id'

# 使用视图集
class BooksViewSet(ModelViewSet):
    """
    list:
    返回图书列表数据

    retrieve:
    返回图书详情数据

    latest:
    返回最新的图书数据

    read:
    修改图书的阅读量
    """

    # 指定视图集＆序列化器
    queryset = BookInfo.objects.all()
    serializer_class = BookInfoSerializer
    # 认证
    authentication_classes = (SessionAuthentication,BasicAuthentication)

    # 权限
    permission_classes = (IsAuthenticated,)

    # 限流
    throttle_classes = (UserRateThrottle,)
    throttle_scope = 'contacts'  # 可以限制用户对于每个视图的访问频次,使用IP或user_id

    # 指定过滤字段
    filter_fields = ('id', 'btitle', 'bread')

    # 排序
    filter_backends = [OrderingFilter]  # 指定排序的方法
    ordering_fields = ('id', 'btitle', 'bread')

    # 分页
    pagination_class = Page

    @action(methods=['get'], detail=False)
    def latest(self, request):

        # 返回最新图书信息
        book = BookInfo.objects.latest('pk')
        bookser = self.get_serializer(book)

        return Response(bookser.data)




