from rest_framework import serializers

from books.models import BookInfo, HeroInfo


def about_django(value):
    if 'django' not in value.lower():
        raise serializers.ValidationError('图书不是关于python的')
    return value


class HeroInfoSerializer(serializers.ModelSerializer):
    """英雄数据序列化器"""

    class Meta:
        model = HeroInfo
        fields = '__all__'
        # depth = 1


class BookInfoSerializer(serializers.ModelSerializer):
    """图书数据序列化器"""
    # name = serializers.CharField()
    heroinfo_set = HeroInfoSerializer(many=True)

    class Meta:
        model = BookInfo
        fields = ('id', 'btitle', 'bpub_date', 'bread', 'bcomment', 'heroinfo_set')
        # read_only_fields = ('id', 'bread', 'bcomment')
        # extra_kwargs = {
        #     'bread': {'min_value': 0, 'required': True},
        #     'bcomment': {'min_value': 0, 'required': True},
        # }

    def create(self, validated_data):
        return BookInfo.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.btitle = validated_data.get('btitle', instance.btitle)
        instance.bpub_date = validated_data.get('bpub_date', instance.bpub_date)
        instance.bread = validated_data.get('bread', instance.bread)
        instance.bcomment = validated_data.get('bcomment', instance.bcomment)
        # instance.image = validated_data.get('image', instance.image)
        instance.save()
        return instance
#
# class BookInfoSerializer(serializers.Serializer):
#     """图书数据序列化器"""
#     id = serializers.IntegerField(label='ID', read_only=True)
#     btitle = serializers.CharField(label='名称', max_length=20)  #  validators=[about_django]
#     bpub_date = serializers.DateField(label='发布日期', required=False)
#     bread = serializers.IntegerField(label='阅读量', required=False)
#     bcomment = serializers.IntegerField(label='评论量', required=False)
#     image = serializers.ImageField(label='图片', required=False)
#     heroinfo_set = serializers.PrimaryKeyRelatedField(read_only=True, many=True)
#
#     # def validate_btitle(self, value):
#     #     if 'django' not in value.lower():
#     #         raise serializers.ValidationError('图书不是关于python的!')
#     #     return value
#
#     # def validate(self, attrs):
#     #     bread = attrs['bread']
#     #     bcomment = attrs['bcomment']
#     #     if bread < bcomment:
#     #         raise serializers.ValidationError('图书的阅读量小于评论量')
#     #     return attrs
#
#     def create(self, validated_data):
#         return BookInfo(**validated_data)
#
#     def update(self, instance, validated_data):
#         instance.btitle = validated_data.get('btitle', instance.btitle)
#         instance.bpub_date = validated_data.get('bpub_date', instance.bpub_date)
#         instance.bread = validated_data.get('bread', instance.bread)
#         instance.bcomment = validated_data.get('bcomment', instance.bcomment)
#         instance.image = validated_data.get('image', instance.image)
#         instance.save()
#         return instance


# class HeroInfoSerializer(serializers.Serializer):
#     """英雄数据序列化器"""
#     GENDER_CHOICES = (
#         (0, 'male'),
#         (1, 'female')
#     )
#
#     id = serializers.IntegerField(label='ID', read_only=True)
#     hname = serializers.CharField(label='名字', max_length=20)
#     hcomment = serializers.CharField(label='描述信息', max_length=200, required=False, allow_null=True)
#     bgender = serializers.ChoiceField(choices=GENDER_CHOICES, label='性别', required=False)
#     # hbook = serializers.PrimaryKeyRelatedField(label='图书', queryset=HeroInfo.objects.all())
#     hbook = serializers.StringRelatedField(label='图书', read_only=True)
