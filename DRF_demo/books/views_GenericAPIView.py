from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from rest_framework.mixins import ListModelMixin, CreateModelMixin, RetrieveModelMixin, UpdateModelMixin, DestroyModelMixin

from books.models import BookInfo
from books.serializers import BookInfoSerializer


class BooksView(ListModelMixin,CreateModelMixin,GenericAPIView):

    # 指定查询集和序列化器
    queryset = BookInfo.objects.all()
    serializer_class = BookInfoSerializer

    def get(self, request):
        # 获取全部数据
        # books = self.get_queryset()
        # bookser = self.get_serializer(books, many=True)

        # return Response(bookser.data)
        return self.list(request)

    def post(self, request):
        # 保存数据
        # json_str = request.data
        # print(json_str)
        #
        # bookser = self.get_serializer(data=request.data)
        #
        # bookser.is_valid(raise_exception=True)
        # bookser.save()
        #
        # return Response(bookser.data)
        return self.create(request)


class BookView(DestroyModelMixin, UpdateModelMixin, RetrieveModelMixin, GenericAPIView):

    # 指定查询集和序列化器
    queryset = BookInfo.objects.all()
    serializer_class = BookInfoSerializer

    def get(self,request, pk):
        # 获取单一数据
        # book = self.get_object()
        # book_ser = self.get_serializer(book)
        #
        # return Response(book_ser.data)
        return self.retrieve(request)

    def put(self,request, pk):
        # 修改数据
        # book = self.get_object()
        #
        # book_ser = self.get_serializer(book, data=request.data)
        #
        # book_ser.is_valid(raise_exception=True)
        # book_ser.save()
        #
        # return Response(book_ser.data)
        return self.update(request)

    def delete(self, request, pk):
        return self.destroy(request)