""" URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from rest_framework.routers import DefaultRouter, SimpleRouter

from books import views_APIView, views_GenericAPIView, views

urlpatterns = [
    # 基于APIView
    # url(r'^books/$', views_APIView.BookListView.as_view()),
    # url(r'^books/(?P<pk>\d+)/$', views_APIView.BookView.as_view()),

    # 基于GenericAPIView
    # url(r'^books/$', views_GenericAPIView.BooksView.as_view()),
    # url(r'^books/(?P<pk>\d+)/$', views_GenericAPIView.BookView.as_view()),

    # 子类视图
    # url(r'^books/$', views.BooksView.as_view()),
    # url(r'^books/(?P<pk>\d+)/$', views.BookView.as_view()),

    # 使用视图集
    # url(r'^books/$', views.BooksViewSet.as_view({'get': 'list', 'post': 'create'})),
    # url(r'^books/latest/$', views.BooksViewSet.as_view({'get': 'latest'})),
    # url(r'^books/(?P<pk>\d+)/$', views.BooksViewSet.as_view({'get': 'retrieve', 'put': 'update', 'delete': 'destroy'})),
]

# 注册视图集
routes = SimpleRouter()
routes.register(r'books', views.BooksViewSet, base_name='book')

# 添加路由数据
urlpatterns += routes.urls
